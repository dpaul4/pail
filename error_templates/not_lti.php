<h1>Error</h1>
<p>Please connect through a <a href="https://en.wikipedia.org/wiki/Learning_management_system">Learning Management System</a>
  using  <a href="https://en.wikipedia.org/wiki/Learning_Tools_Interoperability">Learning Tools Interoperability</a>.</p>

<p>If you are unsure what to do, please contact <a href="mailto:<?php echo $pail_contact; ?>"><?php echo $pail_contact; ?></a>.</p>
