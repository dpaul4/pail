import sys
import os
import random
from string import Template

# Creates display.md that is world-readable, and answers.md that is readable only by the owner
# in the directory specified in the last command line argument passed to the program


def create_template_from_string(string_in):
    """ Creates a simple string Template from the given string """
    return Template(string_in)


def create_template_from_file(file):
    """ Reads in the contents of the given file and uses it to create a simple string Template """
    with open(file, "r") as file_in:
        string_in = file_in.read()
        return create_template_from_string(string_in)


def output_file_from_template(file_out, template, substitutions = {}, permissions = 0o444):
    """
        Uses the given substitutions with the given template to generate content to be written to file_out,
        created with the given permissions.

        Params:
          file_out The file to write content out to
          template The template used to write to file_out
          substitutions The substitutions to use with the template (defaults to the empty dictionary)
          permissions The permissions to give to the specified file (defaults to world-wide readonly)
    """
    contents = template.substitute(substitutions)
    with open(os.open(file_out, os.O_CREAT | os.O_WRONLY, permissions), "w") as out:
        out.write(contents)



if __name__ == "__main__":
    directory = sys.argv[-1]
    display = directory + "display.md"
    answers = directory + "answers.md"

    display_template = create_template_from_string("# Simple *Markdown* Test\nThis is the default display file, which contains a random value ${x}")
    answer_template = create_template_from_string("# Simple *Markdown* test\nThis is a hiddent answer file.\nMake is readable by the PHP server to allow PAIL to display it.\n It contains the same random value as the display: ${x}")

    substitutions = {"x": random.randint(1, 10)}

    output_file_from_template(display, display_template, substitutions)
    # Make answers.md only readable by the owner
    output_file_from_template(answers, answer_template, substitutions, 0o400)

