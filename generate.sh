#!/usr/bin/env bash

set -Eeuo pipefail
trap cleanup SIGINT SIGTERM ERR EXIT

usage() {
  cat <<EOF
Usage: $(basename "${BASH_SOURCE[0]}") [-h] [-v] -c context_value -u user_value command_args

Script to personalise assessments for individualised learning

Available options:

-h, --help      Print this help and exit
-v, --verbose   Print script debug info
-c, --context   The context id to generate files for
-u, --user      The user id to generate files for
command_args    The command to execute to generate files -
                it will have the directory where files should be generated passed as its final parameter
EOF
  exit
}

main() {
  # determine directory (creating if necessary) to pass to the command
  local directory="generated/${context}/${user}/"
  info "Generating files in ${directory}"
  mkdir -p ${directory}
  # add directory to command line arguments and execute command passed as arguments
  args+=("${directory}")
  "${args[@]}"
  success "Successfully generated files in ${directory}"
}

parse_params() {
  # default values of variables set from params
  context=''
  user=''

  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -v | --verbose) set -x ;;
    --no-color) NO_COLOR=1 && setup_colors ;;
    -c | --context)
      context="${2-}"
      shift
      ;;
    -u | --user)
      user="${2-}"
      shift
      ;;
    -?*) die "Unknown option: $1" ;;
    *) break ;;
    esac
    shift
  done

  args=("$@")

  # check required params and arguments
  [[ -z "${context-}" ]] && die "Missing required parameter: context"
  [[ -z "${user-}" ]] && die "Missing required parameter: user"
  [[ "${context-}" =~ ^[[:alnum:]]+$ ]] || die "context must be alphanumeric only"
  [[ "${user-}" =~ ^[[:alnum:]]+$ ]] || die "user must be alphanumeric only"
  [[ ${#args[@]} -eq 0 ]] && die "No command specified"

  return 0
}

cleanup() {
  trap - SIGINT SIGTERM ERR EXIT
  warn "Performing cleanup"
  # script cleanup here
}

setup_colors() {
  if [[ -t 2 ]] && [[ -z "${NO_COLOR-}" ]] && [[ "${TERM-}" != "dumb" ]]; then
    NOFORMAT='\033[0m' RED='\033[0;31m' GREEN='\033[0;32m' ORANGE='\033[0;33m' BLUE='\033[0;34m' PURPLE='\033[0;35m' CYAN='\033[0;36m' YELLOW='\033[1;33m'
  else
    NOFORMAT='' RED='' GREEN='' ORANGE='' BLUE='' PURPLE='' CYAN='' YELLOW=''
  fi
}

msg() {
  echo >&2 -e "${1-}"
}

success() {
  msg "${GREEN}Success:${NOFORMAT} ${1}"
}

info() {
  msg "${BLUE}Info:${NOFORMAT} ${1}"
}

warn() {
  msg "${ORANGE}Warning:${NOFORMAT} ${1}"
}

error() {
  msg "${RED}Error:${NOFORMAT} ${1}"
}

die() {
  local msg=$1
  local code=${2-1} # default exit status 1
  error "$msg"
  exit "$code"
}

script_dir=$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd -P)

setup_colors
parse_params "$@"
main "$@"

